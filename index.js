const express=require("express");
const app=express();
const {v4}=require('uuid');

app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(express.static('public'))

app.get('/api',(req,res)=>{
    const path=`/api/item/${v4()}`
    res.setHeader('Content-Type','text/html');
    res.setHeader('Cache-Control','s-max-age=1,state-while-revalidate');
    res.end(`hello! Go to item: <a href="${path}">${path}</a>`)
})
app.get('/api/item/:slug',(req,res)=>{
    const {slug}=req.params;
    res.end(`item:${slug}`)
})

app.listen(process.env.PORT||3000,()=>{
    console.log(`Server started`)
})